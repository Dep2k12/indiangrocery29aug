
//  Created by Pranav Bhardwaj.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol CoreLocationControllerDelegate
@required

- (void)locationUpdate:(CLLocation *)location;
- (void)locationError:(NSError *)error;


@optional
-(void)locationDirectionUpdated:(CLHeading *)newHeading;


@end


@interface CoreLocationController : NSObject <CLLocationManagerDelegate> {
	CLLocationManager *locMgr;						
	id __unsafe_unretained delegate;
}

@property (nonatomic, strong) CLLocationManager *locMgr;
@property (nonatomic, unsafe_unretained) id delegate;

@end
