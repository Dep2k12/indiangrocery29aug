//
//  Store.m
//  IndianShopping
//
//  Created by Jagsonics on 19/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Store.h"

@implementation Store



@synthesize storeName;
@synthesize storeProductName;
@synthesize storeProductQuantity;
@synthesize storeProductSaving;
@synthesize storeProductPrice;
@synthesize storeId;
@synthesize storeProductId;
@synthesize distance;

// could be optional
@synthesize storeAddress;

@synthesize storeLatitude;
@synthesize storeLongitude;



- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    
    [encoder encodeObject:self.storeName forKey:@"storeNameKey"];
    [encoder encodeObject:self.storeProductName forKey:@"storeProductNameKey"];
    [encoder encodeObject:self.storeProductPrice forKey:@"storeProductPriceKey"];
    [encoder encodeInteger:self.storeProductQuantity forKey:@"storeProductQuantityKey"];
    [encoder encodeObject:self.storeProductSaving forKey:@"storeProductSavingKey"];
    [encoder encodeInteger:self.storeProductId forKey:@"storeProductId"];
    [encoder encodeInteger:self.storeId forKey:@"storeIdKey"];
  
    //could be optional
    
    [encoder encodeObject:self.storeAddress forKey:@"storeAddressKey"];

    [encoder encodeObject:self.storeLatitude forKey:@"storeLatitudeKey"];
    [encoder encodeObject:self.storeLongitude forKey:@"storeLongitudeKey"];
    }

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        
        
        self.storeName = [decoder decodeObjectForKey:@"storeNameKey"];
        self.storeProductName = [decoder decodeObjectForKey:@"storeProductNameKey"];
        self.storeProductPrice = [decoder decodeObjectForKey:@"storeProductPriceKey"];
        self.storeProductQuantity = [decoder decodeIntegerForKey:@"storeProductQuantityKey"];
        self.storeProductSaving = [decoder decodeObjectForKey:@"storeProductSavingKey"];
        self.storeId = [decoder decodeIntegerForKey:@"storeIdKey"];
        self.storeProductId = [decoder decodeIntegerForKey:@"storeProductId"];
       
        
        // could be optional
        self.storeAddress = [decoder decodeObjectForKey:@"storeAddressKey"];
        
        self.storeLatitude = [decoder decodeObjectForKey:@"storeLatitudeKey"];
        self.storeLongitude = [decoder decodeObjectForKey:@"storeLongitudeKey"];
        
    }
    return self;
}






@end
