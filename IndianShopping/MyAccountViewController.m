//
//  MyAccountViewController.m
//  IndianShopping
//
//  Created by Jagsonics on 09/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MyAccountViewController.h"
#import "SBJson.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDImageCache.h>

//------------getBuyerAccountDetail output paramters---------
#define GBAD_REWARD_POINTS   @"reward_points"
#define GBAD_QRCODE_URL     @"qr"

@interface MyAccountViewController ()

@end

@implementation MyAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    dispatch_async(kBgQueue, ^{
        
        NSString *userId = [[NSUserDefaults standardUserDefaults] valueForKey:@"userId"];
        //first_name, last_name,userName, userPassword, phone_number, address
        NSString *urlStringWithParameters = [NSString stringWithFormat:@"%@?user_id=%@",getBuyerAccountDetailUrl,userId];
        
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString: urlStringWithParameters] options:0 error:&error];
        glbNetworkError = error;
        [self performSelectorOnMainThread:@selector(fetchedAccountData:) 
                               withObject:data waitUntilDone:YES];
    });
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    
}

- (void)fetchedImageData:(NSData *)responseData 
{
   
    UIImage *qrImage = [[UIImage alloc]initWithData:responseData];
    [qrcodeImageView setImage:qrImage];
}

-(void) loadImageInBackground {  

// Create a pool  
    @autoreleasepool {
        
        NSLog(@"url=%@",imageURL);
        NSURL *imgURL = [NSURL URLWithString:imageURL];
        NSData *imgData = [NSData dataWithContentsOfURL:imgURL];  
        UIImage *img    = [[UIImage alloc] initWithData:imgData];  
        
        // Image retrieved, call main thread method to update image, passing it the downloaded UIImage  
        [self performSelectorOnMainThread:@selector(assignImageToImageView:) withObject:img waitUntilDone:YES];
    }

// Retrieve the remote image  
  
}
-(void)assignImageToImageView:(UIImage *)image
{
    [qrcodeImageView setImage:image]; 
}
- (void)fetchedAccountData:(NSData *)responseData {
    
    //parse out the json data
    
    if (glbNetworkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There is some problem with the network" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
        
    }
    else {
        
        //{"user_id":"23","email":"z@tmail.com","first_name":"zzzzzzz","last_name":"Zzzz"}
        //{"error":"Login Failed"}
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSArray *arr= [parser objectWithData:responseData];
        NSDictionary *dict = [arr objectAtIndex:0];
        
        
        NSLog(@"dict=%@",dict);
        //NSArray *arr = [parser objectWithData:responseData];
       // NSLog(@"arra=%@",arr);
                
        if (error == nil)
        {
         NSString *str = [dict valueForKey: GBAD_QRCODE_URL];          
           // imageURL = [dict valueForKey: GBAD_QRCODE_URL];          
            NSString *rewardPoints = (NSString *)[dict valueForKey:GBAD_REWARD_POINTS];
            
          
            NSLog(@"rewardPonts=%@",rewardPoints);
            
            if(rewardPoints ==(id)[NSNull null])
               {
                   [rewardPointsLabel setText:[NSString stringWithFormat:@"Reward points: 0"]];               }
               else
               {
                   [rewardPointsLabel setText:[NSString stringWithFormat:@"Reward points: %@",rewardPoints]];
               }
           
                     
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:str]];
    UIImage *image = [UIImage imageWithData:data];
    [qrcodeImageView setImage:image];
    [activityIndicator stopAnimating];
    
            
        }  
        
        
    }
    
       
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
