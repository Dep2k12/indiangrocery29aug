//
//  SortedShoppingListCell.h
//  IndianShopping
//
//  Created by Jagsonics on 25/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SortedShoppingListCell : UITableViewCell
{
    IBOutlet UILabel *store;
    IBOutlet UILabel *product;
    IBOutlet UILabel *quantity;
    IBOutlet UILabel *price;
    IBOutlet UILabel *saving;
}

@property(nonatomic,strong) IBOutlet UILabel *store;
@property(nonatomic,strong) IBOutlet UILabel *product;
@property(nonatomic,strong) IBOutlet UILabel *quantity;
@property(nonatomic,strong) IBOutlet UILabel *price;
@property(nonatomic,strong) IBOutlet UILabel *saving;


@end
