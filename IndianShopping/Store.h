//
//  Store.h
//  IndianShopping
//
//  Created by Jagsonics on 19/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Store : NSObject
{
    NSString *storeName;
    NSString *storeProductName;
    
    NSInteger storeProductQuantity;
    NSString *storeProductSaving;
    NSString *storeProductPrice;
    
    NSInteger storeId;
    NSInteger storeProductId;
    
    // delete these if not required later on
    NSString *storeAddress;
    
    NSString *storeLatitude;
    NSString *storeLongitude;
    CGFloat distance;
   
  
}

@property(nonatomic,strong) NSString *storeName;
@property(nonatomic,strong) NSString *storeProductName;
@property(nonatomic) NSInteger storeProductQuantity;
@property(nonatomic,strong) NSString *storeProductSaving;
@property(nonatomic,strong) NSString *storeProductPrice;
@property(nonatomic) NSInteger storeId;
@property(nonatomic) NSInteger storeProductId;

@property(nonatomic,strong) NSString *storeAddress;
@property(nonatomic,strong) NSString *storeLatitude;
@property(nonatomic,strong) NSString *storeLongitude;
@property CGFloat distance;


@end
