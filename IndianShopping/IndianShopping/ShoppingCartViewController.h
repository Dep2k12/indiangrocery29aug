//
//  ShoppingCartViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 22/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingCartViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *shoppingListArray;
    NSInteger totalCost;
    
    IBOutlet UILabel * totalCostLabel;
    IBOutlet UITableView *shoppingListTable;
}

-(IBAction)finalizeMyList:(id)sender;

@end
