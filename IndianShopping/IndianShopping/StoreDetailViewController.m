//
//  ProductListViewController.m
//  IndianShopping
//
//  Created by Jagsonics on 18/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "StoreDetailViewController.h"
#import <QuartzCore/Quartzcore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDImageCache.h>
#import "SBJson.h"
#import "AppDelegate.h"
@interface StoreDetailViewController ()

@end

#define X_ONE 15
#define Y_ALL 5
#define IMAGE_HEIGHT 65
#define IMAGE_WIDTH 65
#define IMAGE_ONE_TAG 1
#define IMAGE_TWO_TAG 2
#define IMAGE_THREE_TAG 3
#define IMAGE_FOUR_TAG 4

@implementation StoreDetailViewController
@synthesize storeName;
@synthesize storeId;
@synthesize navButtonTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"StoreName =%@",storeName);
    NSLog(@"StoreId = %d",storeId);
    
    [self callDefaultServicesAsynchronously];
    
    UIImage* navBtnImage = [UIImage imageNamed:@"NavButtonBg"];
    CGRect frameimg = CGRectMake(0, 0, navBtnImage.size.width, navBtnImage.size.height);
    UIButton *leftBtn = [[UIButton alloc] initWithFrame:frameimg];
    //[leftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [leftBtn setTitle:navButtonTitle forState:UIControlStateNormal];
    leftBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [leftBtn setBackgroundImage:navBtnImage forState:UIControlStateNormal];
    
    [leftBtn addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
       [self.navigationController.navigationBar setTintColor:[((AppDelegate*)[[UIApplication sharedApplication]delegate ])colorWithHexString:@"C20907" ]];
    
    
    self.navigationItem.title = storeName;
}

-(void)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void) callDefaultServicesAsynchronously
{
    dispatch_async(kBgQueue, ^{
        
        //from user defaults
               
        
        NSString *urlStringWithParameters = [NSString stringWithFormat:@"%@?store_id=%d",getStoreDetailUrl,storeId];      
                                             
                                             
     NSLog(@"url:%@",urlStringWithParameters);
        
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString: urlStringWithParameters] options:0 error:&error];
        glbNetworkError = error;
        [self performSelectorOnMainThread:@selector(fetchedStoreDetailData:) 
                               withObject:data waitUntilDone:YES];
    });
    
}

- (void)fetchedStoreDetailData:(NSData *)responseData {
    
    
    [activityIndicator stopAnimating];
    //parse out the json data
    
    if (glbNetworkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There is some problem with the network" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *json_string = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSArray *json = [parser objectWithString:json_string error:&error]; 
        
        NSLog(@"Error=%@",error);
        NSLog(@"Featured Product Response:%@",json);            
        if (error == nil)
        {
                       
            NSLog(@"getStoreDetail response=%@",json);
            productsArray = [json valueForKey:STORE_PRODUCTS];  
            NSLog(@"Store Products=%@",productsArray);
            NSLog(@"Products Array Count=%d",[productsArray count]);
            
            
            [productsTableView reloadData];   
        }
        
        
        
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // change it later
    NSLog(@"ProductsArray count=%d",[productsArray count]);
    return [productsArray count]/4;      
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
    
    UIImageView *product_one;
    UIImageView *product_two;
    UIImageView *product_three;
    UIImageView *product_four;
    
    NSLog(@"Index Path=%d",indexPath.row);
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        
        product_one = [[UIImageView alloc] initWithFrame:CGRectMake(X_ONE,Y_ALL, IMAGE_WIDTH , IMAGE_HEIGHT)];
        product_two = [[UIImageView alloc] initWithFrame:CGRectMake(X_ONE+IMAGE_WIDTH+10,Y_ALL, IMAGE_WIDTH , IMAGE_HEIGHT)];
        product_three = [[UIImageView alloc] initWithFrame:CGRectMake(X_ONE+2*IMAGE_WIDTH+2*10,Y_ALL, IMAGE_WIDTH , IMAGE_HEIGHT)];
        product_four = [[UIImageView alloc] initWithFrame:CGRectMake(X_ONE+3*IMAGE_WIDTH+3*10,Y_ALL, IMAGE_WIDTH , IMAGE_HEIGHT)];  
        
        UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)] ;
        UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)] ;
        UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)] ;
        UITapGestureRecognizer *tapGesture4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)] ;
        
        //tapGesture.numberOfTapsRequired = 1;
        
        product_one.userInteractionEnabled = YES;
        product_two.userInteractionEnabled = YES;
        product_three.userInteractionEnabled = YES;
        product_four.userInteractionEnabled = YES;
        
        
        [product_one addGestureRecognizer:tapGesture1];
        [product_two addGestureRecognizer:tapGesture2];
        [product_three addGestureRecognizer:tapGesture3];
        [product_four addGestureRecognizer:tapGesture4];
        
        
        
        
        product_one.tag = IMAGE_ONE_TAG;
        product_two.tag = IMAGE_TWO_TAG;
        product_three.tag = IMAGE_THREE_TAG;
        product_four.tag = IMAGE_FOUR_TAG;
        
        
      //  product_one.layer.cornerRadius = 5;
        product_one.layer.borderWidth = 1;
        product_one.layer.borderColor = [UIColor blackColor].CGColor;
        
      //  product_two.layer.cornerRadius = 5;
        product_two.layer.borderWidth = 1;
        product_two.layer.borderColor = [UIColor blackColor].CGColor;
        
      //  product_three.layer.cornerRadius = 5;
        product_three.layer.borderWidth = 1;
        product_three.layer.borderColor = [UIColor blackColor].CGColor;
        
       // product_four.layer.cornerRadius = 5;
        product_four.layer.borderWidth =1;
        product_four.layer.borderColor = [UIColor blackColor].CGColor;
        
        
        product_one.contentMode = UIViewContentModeScaleToFill;
        product_two.contentMode = UIViewContentModeScaleToFill;
        product_three.contentMode =UIViewContentModeScaleToFill;
        product_four.contentMode = UIViewContentModeScaleToFill;
        
        
        
        
        [cell.contentView addSubview:product_one];
        [cell.contentView addSubview:product_two];
        [cell.contentView addSubview:product_three];
        [cell.contentView addSubview:product_four];
        
    }
    
    else {
        product_one = (UIImageView *)[cell.contentView viewWithTag:IMAGE_ONE_TAG];
        product_two = (UIImageView *)[cell.contentView viewWithTag:IMAGE_TWO_TAG];
        product_three = (UIImageView *)[cell.contentView viewWithTag:IMAGE_THREE_TAG];
        product_four = (UIImageView *)[cell.contentView viewWithTag:IMAGE_FOUR_TAG];
        
    }
   
    
    NSLog(@"indexPath.row = %d",indexPath.row);
    NSLog(@"tag1=%d \n tag2=%d \n tag3=%d \n tag4=%d \n",product_one.tag,product_two.tag,product_three.tag,product_four.tag);
    
    int index1 = indexPath.row*4 + product_one.tag - 1;
    int index2 = indexPath.row*4 + product_two.tag - 1;
    int index3=  indexPath.row*4 + product_three.tag - 1;
    int index4 = indexPath.row*4 + product_four.tag - 1;
    
    
    NSLog(@"index1:%d \n index2=%d \n index3=%d \n index4=%d \n",index1,index2,index3,index4);
    // NSLog(@"productsArray=%@",productsArray);
    if (indexPath.row < (([productsArray count]+3)/4 - 1) )// until second last row, fetch all entries
    {
        // fetch all entries
        
        NSString *imageUrl1 = [((NSDictionary*)[productsArray objectAtIndex:(index1)]) valueForKey:STORE_PRODUCT_IMAGE_URL];
        
        [product_one  setImageWithURL:[NSURL URLWithString: imageUrl1]                          placeholderImage:[UIImage imageNamed:@"photofram"]]; 
        
        
        NSString *imageUrl2 = [((NSDictionary *)[productsArray objectAtIndex:(index2)]) valueForKey:STORE_PRODUCT_IMAGE_URL];
        
        [product_two  setImageWithURL:[NSURL URLWithString: imageUrl2]                          placeholderImage:[UIImage imageNamed:@"photofram"]];  
        
        
        NSString *imageUrl3 = [((NSDictionary *)[productsArray objectAtIndex:(index3)]) valueForKey:STORE_PRODUCT_IMAGE_URL];
        
        [product_three  setImageWithURL:[NSURL URLWithString: imageUrl3]                          placeholderImage:[UIImage imageNamed:@"photofram"]]; 
        
        NSString *imageUrl4 = [((NSDictionary *)[productsArray objectAtIndex:(index4)]) valueForKey:STORE_PRODUCT_IMAGE_URL];
        
        [product_four  setImageWithURL:[NSURL URLWithString: imageUrl4]                          placeholderImage:[UIImage imageNamed:@"photofram"]]; 
    }
    else if(indexPath.row == (([productsArray count]+3)/4 - 1) )    // last row
    {
        
        [product_one setHidden:YES];
        [product_two setHidden:YES];
        [product_three setHidden:YES];
        [product_four setHidden:YES];
        
        switch ([productsArray count]%4) {
            case 0:
                // show all four
            {
                NSString *imageUrl4 = [((NSDictionary *)[productsArray objectAtIndex:(index4)]) valueForKey:STORE_PRODUCT_IMAGE_URL];
                [product_four setHidden:NO];
                [product_four  setImageWithURL:[NSURL URLWithString: imageUrl4]                          placeholderImage:[UIImage imageNamed:@"photofram"]]; 
            }
                
                
            case 3:
                
                // show only 3
            {
                NSString *imageUrl3 = [((NSDictionary *)[productsArray objectAtIndex:(index3)]) valueForKey:STORE_PRODUCT_IMAGE_URL];
                [product_three setHidden:NO];
                [product_three  setImageWithURL:[NSURL URLWithString: imageUrl3]                          placeholderImage:[UIImage imageNamed:@"photofram"]]; 
            }
                
            case 2:
                //show first two
            {
                
                NSString *imageUrl2 = [((NSDictionary *)[productsArray objectAtIndex:(index2)]) valueForKey:STORE_PRODUCT_IMAGE_URL];
                [product_two setHidden:NO];
                [product_two  setImageWithURL:[NSURL URLWithString: imageUrl2]                          placeholderImage:[UIImage imageNamed:@"photofram"]];  
            }
                
            case 1:
                
            {
                
                // show first 1
                
                NSString *imageUrl1 = [((NSDictionary*)[productsArray objectAtIndex:(index1)]) valueForKey:STORE_PRODUCT_IMAGE_URL];
                [product_one setHidden:NO];
                [product_one  setImageWithURL:[NSURL URLWithString: imageUrl1]                          placeholderImage:[UIImage imageNamed:@"photofram"]]; 
                //
                
            }
                
                break;
                
            default:
                break;
        }
        // add only required
    }
    

    
        
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}


- (void)imageTapped:(UITapGestureRecognizer *)gesture 

{
    UIImageView *selectedProduct = ((UIImageView *)[gesture view]);
    UITableViewCell *cell = (UITableViewCell*)[[[gesture view]superview]superview];
    NSIndexPath *indexPath =  [productsTableView indexPathForCell:cell];
    int row = indexPath.row;
    
    int arrayElementIndex = row * 4 + selectedProduct.tag - 1;
    NSString *selectedImageId = [[productsArray objectAtIndex:arrayElementIndex]valueForKey:STORE_PRODUCT_ID];
    
    NSLog(@"selectedImageId=%@",selectedImageId);
    
    NSInteger productId = [selectedImageId intValue];
    
    ProductDetailViewController *cntrl = [[ProductDetailViewController alloc]initWithNibName:nil bundle:nil];
    cntrl.productId = productId;
    cntrl.navButtonTitle=@"Store";
    [self.navigationController pushViewController:cntrl animated:YES];
    NSLog(@"tag=%d",selectedProduct.tag);
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
