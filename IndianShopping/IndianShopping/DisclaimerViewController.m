//
//  DisclaimerViewController.m
//  IndianShopping
//
//  Created by Jagsonics on 29/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DisclaimerViewController.h"
#import "AppDelegate.h"

@interface DisclaimerViewController ()

@end

@implementation DisclaimerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //self.title = NSLocalizedString(@"Disclaimer", @"Disclaimer");
        self.tabBarItem.image = [UIImage imageNamed:@"Disclaimer_selected"];
        self.tabBarItem.title = @"Disclaimer";
       
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
  
    self.navigationItem.title = @"Disclaimer";
     [self.navigationController.navigationBar setTintColor:[((AppDelegate*)[[UIApplication sharedApplication]delegate ])colorWithHexString:@"C20907" ]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
