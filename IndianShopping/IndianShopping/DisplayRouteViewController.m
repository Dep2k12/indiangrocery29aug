//
//  DisplayRouteViewController.m
//  IndianShopping
//
//  Created by Jagsonics on 11/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DisplayRouteViewController.h"
#import "SBJson.h"
#import <AddressBookUI/ABAddressFormatting.h>

@interface DisplayRouteViewController ()

@end

@implementation DisplayRouteViewController
@synthesize points;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

-(void)notifReceived:(NSNotification *)notif
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifReceived:) name:@"popMap" object:nil];
    
    map = [[MQMapView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:map];
    
    uniqueStoresLocations = points;
    NSLog(@"unique stores locations=%@",uniqueStoresLocations);
    
    /*   uniqueStoresLocations= [[NSMutableArray alloc] init];
     
     CLLocation *loc3 = [[CLLocation alloc]initWithLatitude:33.18 longitude:-117.2];
     [uniqueStoresLocations addObject:loc3];
     
     CLLocation *loc2 = [[CLLocation alloc]initWithLatitude:33.2 longitude:-117.1];
     [uniqueStoresLocations addObject:loc2];
     
     
     CLLocation *loc1 = [[CLLocation alloc]initWithLatitude:33.008 longitude:-117];
     [uniqueStoresLocations addObject:loc1];
     
     
     CLLocation *loc4 = [[CLLocation alloc]initWithLatitude:33.2 longitude:-117.22];
     [uniqueStoresLocations addObject:loc4];
     [uniqueStoresLocations addObject:loc1];*/
    
    [self shortestDistanceRoute];
    
    
}




-(void)shortestDistanceRoute
{
    //{locations:[{latLng:{lat:51.524410144966154,lng:-0.12989273652335526}},{latLng:{lat:51.54495915136182,lng:-0.16518885449221493}},{latLng:{lat:51.52061842826141,lng:-0.1495479641837033}},{latLng:{lat:51.52850609658769,lng:-0.20170525707760403}}]};
    
    NSLog(@"coordinates=%@",uniqueStoresLocations);
    
    NSMutableArray *unOptimisedCordArray = [[NSMutableArray alloc]initWithCapacity:[points count]];
    
    for(int v=0;v<[uniqueStoresLocations count];v++)
    {
        CLLocation *point = (CLLocation *)[uniqueStoresLocations objectAtIndex:v];    // it is my current location
        CLLocationCoordinate2D cord = point.coordinate;
        CLLocationDegrees lat = cord.latitude;
        CLLocationDegrees lng = cord.longitude;
        
        NSString *latString = [NSString stringWithFormat:@"%f",lat];
        NSString *lngString = [NSString stringWithFormat:@"%f",lng];
        
        
        NSLog(@"latString=%@ & lngString=%@",latString,lngString);
        NSMutableDictionary *latLngDict = [[NSMutableDictionary alloc]init];
        
        [latLngDict setValue:latString forKey:@"lat"];
        [latLngDict setValue:lngString forKey:@"lng"];
        
        // NSString *jsonOfLatLongDict = [latLngDict JSONRepresentation];
        
        
        NSMutableDictionary *latLngPairDict = [[NSMutableDictionary alloc]init];
        [latLngPairDict setObject:latLngDict forKey:@"latLng"];
        
        [unOptimisedCordArray addObject:latLngPairDict];
        
    }
    
    NSDictionary *jsonDict = [NSDictionary dictionaryWithObject:unOptimisedCordArray forKey:@"locations"];
    NSString *jsonRequest = [jsonDict JSONRepresentation];
    
    NSLog(@"jsonRequest is %@", jsonRequest);
    
    
    NSString *urlStrl = @"http://open.mapquestapi.com/directions/v1/optimizedroute";
    NSURL *url = [NSURL URLWithString:urlStrl];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    
    NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    if (connection) {
        responseData = [NSMutableData data] ;
    } 
}





- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data 
{
    
    [responseData appendData:data];
    
    // NSLog(@"responseData=%@",responseData);
    
}



- (void)connectionDidFinishLoading:(NSURLConnection *)connection{    
    NSError *error;
    NSDictionary* json = [NSJSONSerialization 
                          JSONObjectWithData:responseData 
                          
                          options:kNilOptions 
                          error:&error];
    
    //  NSLog(@"json Response=%@",json);
    
    NSDictionary *route = [json valueForKey:@"route"];
    NSArray *locations = [route valueForKey:@"locations"];
    NSLog(@"Locations=%@",locations);
    NSMutableArray *optimisedCoordArray = [[NSMutableArray alloc]init];
    for(NSDictionary *location in locations)
    {
        NSDictionary *latLngDict = [location valueForKey:@"displayLatLng"];
        CLLocationDegrees optLat = [[latLngDict valueForKey:@"lat"] floatValue];
        CLLocationDegrees optLng = [[latLngDict valueForKey:@"lng"]floatValue];
        CLLocation *optimisedLocation = [[CLLocation alloc] initWithLatitude:optLat longitude:optLng];
        [optimisedCoordArray addObject:optimisedLocation];
        
    }
    
    
    
    
    routeLoadCount = 0;
    routesArray  = [[NSMutableArray alloc]initWithCapacity:([points count] - 1) ];
    
    
	// Do any additional setup after loading the view.
    //  NSLog(@"Points=%@",points);
    if([locations count] > 0)
    {
        for(int j=0;j<[points count] - 1;j++)
        {
            MQRoute *route = [[MQRoute alloc]init];
            route.delegate = self;
            route.mapView = map;
            route.bestFitRoute = true;
            route.routeType = MQRouteTypeShortest;
            
            
            CLLocation *startLoc = [optimisedCoordArray objectAtIndex:j];
            CLLocation *endLoc = [optimisedCoordArray objectAtIndex:j+1];
            
            
            [route getRouteWithStartCoordinate:startLoc.coordinate endCoordinate:endLoc.coordinate];
            
            [routesArray addObject:route];
        }
        
        
        
    }
    else {
        UIAlertView *noRouteAlert = [[UIAlertView alloc]initWithTitle:nil message:@"Cannot find route" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [noRouteAlert show];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)routeLoadError:(NSString *)error
{
    
    
    NSLog(@"Route load Error=%@",error);
}
-(void)routeLoadFinished
{
    // get the raw xml passed back from the server:
    //NSLog(@"%@", route.rawXML);
    BOOL finished = NO;
    
    
    routeLoadCount = routeLoadCount+1;
    // NSLog(@"RouteLoadCount=%d  and routeArrayCount=%d",routeLoadCount,[routesArray count]);
    if(routeLoadCount == [routesArray count])
    {
        finished = YES;
    }
    if(finished)
    {
        /*CGFloat distance = 0;
         for(int i=0;i<[routesArray count];i++)
         {
         MQRoute *route = (MQRoute*)[routesArray objectAtIndex:i];
         // do something with all the maneuvers
         for ( MQManeuver *maneuver in route.maneuvers )
         {
         //  NSLog(@"%@", maneuver.narrative);
         // NSLog(@"Maneuver Distance=%f",maneuver.distance *1000);
         distance = distance + (maneuver.distance)*1000;
         }
         // NSLog(@"Distance=%f",distance);  
         }
         */
    }    
    
}

@end
