//
//  HomeViewController.m
//  IndianShopping
//
//  Created by Jagsonics on 29/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HomeViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ScanViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDImageCache.h>

#import "ProductDetailViewController.h"
#import "StoreDetailViewController.h"
#import "ProductListViewController.h"
#import "SBjson.h"
#import "CoreLocationController.h"
#define LOCATION_ALERT_TAG 3

@interface HomeViewController ()

@end

@implementation HomeViewController


@synthesize productsTableView;
@synthesize storesTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        sliderValue = 20;
        // self.title = NSLocalizedString(@"Home", @"Home");
        self.tabBarItem.image = [UIImage imageNamed:@"Home_selected.png"];
        self.tabBarItem.title = @"Home";
        
        allFeaturedStores = [[NSMutableArray alloc]init];
        allFeaturedProducts = [[NSMutableArray alloc]init];
        allStores = [[NSMutableArray alloc]init];
        
        delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        
    }
    return self;
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    isFirstCall = YES;
    isNetworkAlertShown = NO;
    
    [allFeaturedStores removeAllObjects];
    [allFeaturedProducts removeAllObjects];
    [productsTableView reloadData];
    [storesTableView reloadData];
    
    [activityIndicator setHidden:NO];
    [activityIndicator startAnimating];
    
    [self getLocation];
  
    
    // when view is reappearing after another view is popped
    [self.navigationController.navigationBar setHidden:NO];
    [self.navigationController.navigationBar addSubview:imageView];

    
    //set the distance paramter for services
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%d",sliderValue] forKey:@"distance"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
            // set the latitude and location and call the services in delegat
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"recentSearchesKey" ]; 
   // [recentSearchesArray removeAllObjects];
    recentSearchesArray = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSLog(@"HOME Recent searches Array from user defaults=%@",recentSearchesArray);
    
    
}

- (void)locationError:(NSError *)error
{
    if(error.code== kCLErrorDenied )
    {
        UIAlertView *noLocationAlert = [[UIAlertView alloc]initWithTitle:@"Enable GPS" message:@"1) Go to Settings \n 2) Core Locations\n3) Indian Grocery\n     4)Turn the switch on" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //  noLocationAlert.tag =  LOCATION_ALERT_TAG;
        [noLocationAlert show];
        
        [activityIndicator stopAnimating];
    }
    
}
- (void)locationUpdate:(CLLocation *)location{
    
    latitude = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    longitude = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    NSLog(@"Latitude=%@ and Longitude=%@",latitude,longitude);
    
    
    [CLController.locMgr stopUpdatingLocation];
    
    
    [allFeaturedStores removeAllObjects];
    [allFeaturedProducts removeAllObjects];
    [allStores removeAllObjects];
    if(isFirstCall)
    {
        [self callDefaultServicesAsynchronously];
        isFirstCall = NO;
    }
    
}



-(void)getLocation
{
    
    CLController = [[CoreLocationController alloc] init];
    CLController.delegate = self;
    
    [CLController.locMgr setDistanceFilter:100];
    [CLController.locMgr startUpdatingLocation];
    
    
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // [self getLocation];
    
    
  /*  [activityIndicator setHidden:NO];
    [activityIndicator startAnimating];
     [self getLocation]; */
    imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"CompareIndianGrocery_title"]];
    [imageView setFrame:CGRectMake(35, 8,250,30)];
    [self.navigationController.navigationBar setTintColor:[delegate colorWithHexString:@"C20907"   ]];
    
    
    sliderValue = 20;
    
    
    popUpView.layer.borderColor = [UIColor redColor].CGColor;
    popUpView.layer.borderWidth = 4;
    popUpView.layer.cornerRadius = 10;
    
    [searchTextFieldBackgrouundView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"SearchBar_bg"]]];
    
    [featuredStoresBgView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"ProTitle_bg"]]];
    [featuredProductBgView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"ProTitle_bg"]]];
    self.productsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.storesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
	CGAffineTransform rotateTable = CGAffineTransformMakeRotation(-M_PI_2);
	productsTableView.transform = rotateTable;
	productsTableView.frame = CGRectMake(0, 65, productsTableView.frame.size.width, productsTableView.frame.size.height);
    
    storesTableView.transform = rotateTable;
    storesTableView.frame = CGRectMake(0, 170, storesTableView.frame.size.width, storesTableView.frame.size.height);
    
    toggleButton.tag = TOGGLE_STATE_FEATURED_STORES;  //showing only featured stores
    
    
    // imageView.center = self.navigationController.navigationBar.center;
    
    
    
}


-(void) callDefaultServicesAsynchronously
{
  
    distance = [[NSUserDefaults standardUserDefaults] valueForKey:@"distance"];
    
    if (toggleButton.tag == TOGGLE_STATE_ALL_STORES)
    {
        [self getAllStores];
    }
    else {
        dispatch_async(kBgQueue, ^{
            
            NSLog(@"Latitude=%@ Longitude=%@",latitude,longitude);
            NSError *error;
            NSString *urlString = [NSString stringWithFormat: @"http://websupplements.net/projects/compare/rest/getFeaturedStores?withInDistance=%@&fromLatitude=%@&fromLongitude=%@",distance,latitude,longitude];
            NSLog(@"urlString=%@",urlString);
            
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString: 
                                                          urlString] options:0 error:&error];
            glbNetworkError = error;
            [self performSelectorOnMainThread:@selector(fetchedFeaturedStoresData:) 
                                   withObject:data waitUntilDone:YES];
            
            
            
        });

    }
       
    
    dispatch_async(kBgQueue, ^{
        
        NSString *urlString = [NSString stringWithFormat: @"http://websupplements.net/projects/compare/rest/getFeaturedProducts?withInDistance=%@&fromLatitude=%@&fromLongitude=%@",distance,latitude,longitude];
        NSLog(@"urlString=%@",urlString);
        
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString: 
                                                      urlString] options:0 error:&error];
        glbNetworkError = error;
        [self performSelectorOnMainThread:@selector(fetchedFeaturedProductsData:) 
                               withObject:data waitUntilDone:YES];
    });
}





- (void)fetchedAllStoresData:(NSData *)responseData {
    
    requestProcessed++;
    if(requestProcessed  >= ALL_REQUESTS)
        [activityIndicator stopAnimating];
    //parse out the json data
    
    if (glbNetworkError != nil)
    {
        if(!isNetworkAlertShown)
        {
            UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There is some problem with the network" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [networkAlert show];
            isNetworkAlertShown  = YES;
        }
       
    }
    else {
        
        
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *json_string = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSArray *json = [parser objectWithString:json_string error:&error];
        
        NSLog(@"All Stores Response:%@",json);
        [allStores removeAllObjects];
        if (error == nil)
        {
            for (int i=0; i<[json count]; i++)
            {
                [allStores addObject:((NSDictionary*)[json objectAtIndex:i]) ];            
            }
            NSLog(@"all stores count=%d",[allStores count]);
            [storesTableView reloadData];
            
        }
        else {
            
            NSLog(@"Parsing problem");
            
        }
        
        
    }
    [toggleButton setEnabled:YES];
    
}
- (void)fetchedFeaturedProductsData:(NSData *)responseData    //feature products
{
    
    
    
    if (glbNetworkError != nil)
    {
        if(!isNetworkAlertShown)
        {
            UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There is some problem with the network" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [networkAlert show];
            isNetworkAlertShown  = YES;
        }
    }
    else {
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *json_string = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSArray *json = [parser objectWithString:json_string error:&error]; 
        NSLog(@"json Response FEATURED PRODUCTS %@", json);
        NSLog(@"Error=%@",error);
       // NSLog(@"Featured Product Response:%@",json);
        // NSMutableArray *images = [[NSMutableArray alloc]init];
        
        [allFeaturedProducts removeAllObjects];
        if (error == nil)
        {
            for (int i=0; i<[json count]; i++)
            {
                //[productsArray addObject:[((NSDictionary*)[json objectAtIndex:i]) objectForKey:@"productImageUrl"]];
                
                
                [allFeaturedProducts addObject:((NSDictionary*)[json objectAtIndex:i])];
                [productsTableView reloadData];
               
                
            }
            
        }
        else 
        {
            
            NSLog(@"Parsing problem");
        }
        
        
        
        
    }
    requestProcessed++;
    if(requestProcessed  >= ALL_REQUESTS)
        [activityIndicator stopAnimating];
    
    
}



- (void)fetchedFeaturedStoresData:(NSData *)responseData     // featured store
{
    
    
    
    if (glbNetworkError != nil)
    {
        if(!isNetworkAlertShown)
        {
            UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There is some problem with the network" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [networkAlert show];
            isNetworkAlertShown  = YES;
        }

    }
    
    else {
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSString *json_string = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSArray *json = [parser objectWithString:json_string error:&error]; 
        
        NSLog(@"Error=%@",error);
        NSLog(@"json Response FEATURED STORES %@", json);
        [allFeaturedStores removeAllObjects];
        
        if (error == nil)
        {
            for (int i=0; i<[json count]; i++)
            {
                [allFeaturedStores addObject:((NSDictionary*)[json objectAtIndex:i]) ];            
            }
            
            NSLog(@"featured stores count=%d",[allFeaturedStores count]);
            [storesTableView reloadData];
            
            
                    }
        else {
            NSLog(@"Parsing problem");   
        }
        
    }
    requestProcessed++; 
    if(requestProcessed  >= ALL_REQUESTS)
        [activityIndicator stopAnimating];
    
    [toggleButton setEnabled:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return rowWidth;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == productsTableView)
    {
        NSLog(@"Array Count=%d",[allFeaturedProducts count]);
        return [allFeaturedProducts count];
      //  return 20;
    }
    else {
        if (toggleButton.tag == TOGGLE_STATE_ALL_STORES)
        {
            // this one will change
            return ceil([allStores count] /2); 
        }
            
        else {
            return ceil([allFeaturedStores count] /2);
        }
       // return 20;
        
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    if (tableView == productsTableView)
    {
        
        UIImageView *productImageView;
        UILabel *footerLabel;
        
        NSLog(@"Index Path=%d",indexPath.row);
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) 
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
            
            productImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5,10, 65, 65)];
            footerLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, 65, 25)];
            footerLabel.backgroundColor = [UIColor blackColor];
            footerLabel.tag =TAG_FOOTER_PRODUCT;
            footerLabel.alpha = .6;
            footerLabel.textColor = [UIColor whiteColor];
            footerLabel.font =  [UIFont systemFontOfSize:10];
            footerLabel.textAlignment = UITextAlignmentCenter;
            footerLabel.numberOfLines = 2;
            footerLabel.lineBreakMode = UILineBreakModeWordWrap;
             
            
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)] ;
            
            [productImageView addGestureRecognizer:tapGesture];
            
            productImageView.userInteractionEnabled = YES;
            productImageView.tag = PRODUCT_IMAGE_TAG;
            
            //  productImageView.layer.cornerRadius = 10;
            productImageView.layer.borderWidth = 1;
            productImageView.layer.borderColor = [UIColor blackColor].CGColor;
            
            
            productImageView.contentMode = UIViewContentModeScaleToFill;
            
            CGAffineTransform rotateImage = CGAffineTransformMakeRotation(M_PI_2);
            productImageView.transform = rotateImage;
            footerLabel.transform = rotateImage;
            footerLabel.frame = CGRectMake(5, 10, 25, 65);
            
            [cell.contentView addSubview:productImageView];
            [cell.contentView addSubview:footerLabel];
            
        }
        
        else 
        {
            productImageView = (UIImageView *)[cell.contentView viewWithTag:PRODUCT_IMAGE_TAG];
            footerLabel = (UILabel *)[cell.contentView viewWithTag:TAG_FOOTER_PRODUCT];
            
        }
//         [productImageView setImage:[UIImage imageNamed:@"Home_selected@2x.png"]];
//        footerLabel.text = @"This is two line text";
       footerLabel.text = [((NSDictionary *)[allFeaturedProducts objectAtIndex:indexPath.row]) valueForKey:PRODUCT_NAME_HVC ];
        
        NSString *imageUrl = [((NSDictionary *)[allFeaturedProducts objectAtIndex:indexPath.row]) valueForKey:PRODUCT_IMAGE_HVC];
        
        NSLog(@"imageUrl=%@",imageUrl);
        
       
       [productImageView  setImageWithURL:[NSURL URLWithString: imageUrl]                          placeholderImage:[UIImage imageNamed:@"photofram"]];
       
                
        return cell;
        
    }
    else {
        
        // bottom tableView
        UIImageView *topImageView;
        UIImageView *bottomImageView;
        UILabel *topFooterLabel;
        UILabel *bottomFooterLabel;
        
        NSLog(@"Index Path=%d",indexPath.row);
        static NSString *CellIdentifier = @"BottomTableCell";
        UITableViewCell *cell = [self.storesTableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
            
            
            topFooterLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 65, 25)];
            topFooterLabel.backgroundColor = [UIColor blackColor];
            topFooterLabel.alpha = .6;
            topFooterLabel.textColor = [UIColor whiteColor];
            topFooterLabel.font =  [UIFont systemFontOfSize:10];         
            topFooterLabel.textAlignment = UITextAlignmentCenter;  
            topFooterLabel.tag = TAG_FOOTER_TOP_IMAGE;
            topFooterLabel.numberOfLines = 2;
            topFooterLabel.lineBreakMode = UILineBreakModeWordWrap;
            
            topImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,10, 65, 65)];
             
            
            topImageView.tag = STORE_TOP_IMAGE;
            topImageView.contentMode = UIViewContentModeScaleToFill;
            //topImageView.layer.cornerRadius = 10;
            topImageView.layer.borderWidth = 1;
            topImageView.layer.borderColor = [UIColor blackColor].CGColor;
            
            UITapGestureRecognizer *topImageTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)] ;
            UITapGestureRecognizer *bottomImagetapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)] ;
            
            
            topImageView.userInteractionEnabled = YES;
            [topImageView addGestureRecognizer:topImageTapGesture];
            
            bottomFooterLabel = [[UILabel alloc] initWithFrame:CGRectMake(75, 10, 65, 25)];
            bottomFooterLabel.backgroundColor = [UIColor blackColor];
            bottomFooterLabel.alpha = .6;
            bottomFooterLabel.textColor = [UIColor whiteColor];
            bottomFooterLabel.font =  [UIFont systemFontOfSize:10];
            bottomFooterLabel.textAlignment = UITextAlignmentCenter; 
            bottomFooterLabel.numberOfLines = 2;
            bottomFooterLabel.lineBreakMode = UILineBreakModeWordWrap;
            bottomFooterLabel.tag = TAG_FOOTEr_BOTTOM_IMAGE;
            
            bottomImageView = [[UIImageView alloc] initWithFrame:CGRectMake(75,10, 65, 65)];
            bottomImageView.tag = STORE_BOTTOM_IMAGE;
            bottomImageView.contentMode = UIViewContentModeScaleToFill;
            //bottomImageView.layer.cornerRadius = 10;
            bottomImageView.layer.borderWidth = 1;
            bottomImageView.layer.borderColor = [UIColor blackColor].CGColor;
            
            [bottomImageView addGestureRecognizer:bottomImagetapGesture];
            bottomImageView.userInteractionEnabled = YES;
            
            
            CGAffineTransform rotateImage = CGAffineTransformMakeRotation(M_PI_2);
            topImageView.transform = rotateImage;
            bottomImageView.transform = rotateImage;
            topFooterLabel.transform = rotateImage;
            bottomFooterLabel.transform = rotateImage;
            
            topFooterLabel.frame = CGRectMake(0, 10, 25, 65);
            bottomFooterLabel.frame = CGRectMake(75, 10, 25, 65);
            
            [cell.contentView addSubview:topImageView];
            [cell.contentView addSubview:bottomImageView];
            [cell.contentView addSubview:topFooterLabel];
            [cell.contentView addSubview: bottomFooterLabel];
            
        }
        
        else {
            topImageView = (UIImageView *)[cell.contentView viewWithTag:STORE_TOP_IMAGE];
            bottomImageView = (UIImageView *)[cell.contentView viewWithTag:STORE_BOTTOM_IMAGE];
            bottomFooterLabel = (UILabel *)[cell.contentView viewWithTag:TAG_FOOTEr_BOTTOM_IMAGE];
            topFooterLabel = (UILabel *)[cell.contentView viewWithTag:TAG_FOOTER_TOP_IMAGE];
            
        }
        
       
        // [topImageView setImage:(UIImage*)[storesArray objectAtIndex:2*indexPath.row];
        
        //  all stores
        if (toggleButton.tag == TOGGLE_STATE_ALL_STORES)
        {
            NSString *topImageUrl = [((NSDictionary *)[allStores objectAtIndex:2*indexPath.row]) valueForKey:STORE_IMAGE_HVC];
            
            NSString *bottomImageUrl = [((NSDictionary *)[allStores objectAtIndex:2*indexPath.row+1]) valueForKey:STORE_IMAGE_HVC];
            
            
            NSLog(@"TopImageUrl=%@",topImageUrl);
            NSLog(@"BottomImageUrl=%@",bottomImageUrl);
            
         [topImageView  setImageWithURL:[NSURL URLWithString:topImageUrl ]
                          placeholderImage:[UIImage imageNamed:@"photofram"]];
            [bottomImageView   setImageWithURL:[NSURL URLWithString:bottomImageUrl]
                              placeholderImage:[UIImage imageNamed:@"photofram"]];
            topFooterLabel.text =  [((NSDictionary *)[allStores objectAtIndex:2*indexPath.row]) valueForKey:STORE_NAME_HVC];
           bottomFooterLabel.text = [((NSDictionary *)[allStores objectAtIndex:2*indexPath.row+1]) valueForKey:STORE_NAME_HVC];

            
//           [topImageView setImage:[UIImage imageNamed:@"Home_selected@2x.png"]];
//            
//            [bottomImageView setImage:[UIImage imageNamed:@"Home_selected@2x.png"]];
//            topFooterLabel.text = @"This is two line text";
//            bottomFooterLabel.text = @"This is two line text";
            
            NSLog(@"All stores");
            
        }
        else {
            NSString *topImageUrl = [((NSDictionary *)[allFeaturedStores objectAtIndex:2*indexPath.row]) valueForKey:STORE_IMAGE_HVC];
            
            NSString *bottomImageUrl = [((NSDictionary *)[allFeaturedStores objectAtIndex:2*indexPath.row+1]) valueForKey:STORE_IMAGE_HVC];
            
            [topImageView  setImageWithURL:[NSURL URLWithString:topImageUrl]
                          placeholderImage:[UIImage imageNamed:@"photofram"]];
            
            [bottomImageView   setImageWithURL:[NSURL URLWithString:bottomImageUrl]
                              placeholderImage:[UIImage imageNamed:@"photofram"]];
            
            topFooterLabel.text =  [((NSDictionary *)[allStores objectAtIndex:2*indexPath.row]) valueForKey:STORE_NAME_HVC];
           bottomFooterLabel.text = [((NSDictionary *)[allStores objectAtIndex:2*indexPath.row+1]) valueForKey:STORE_NAME_HVC];
            
            
                    [bottomImageView   setImageWithURL:[NSURL URLWithString:bottomImageUrl]
                                       placeholderImage:[UIImage imageNamed:@"photofram"]];
            
//            [bottomImageView setImage:[UIImage imageNamed:@"Home_selected@2x.png"]];
//            [topImageView setImage:[UIImage imageNamed:@"Home_selected@2x.png"]];
//            topFooterLabel.text = @"This is two line text";
//            bottomFooterLabel.text = @"This is two line text";
//            NSLog(@"Featured stores");
        }
        
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone ;
        return cell;
    }
}



- (void)imageTapped:(UITapGestureRecognizer *)gesture {
    
    
    
    // image properties should be set in class level vaiables as they are used in 
    // popUpAction methods
    UITableViewCell *cell = (UITableViewCell *)[[[gesture view] superview]superview] ;
    
    UIImageView *selectedImage = (UIImageView *)[gesture view];
    selectedImageTag = selectedImage.tag;
    
    NSIndexPath *indexPath = [self.productsTableView indexPathForCell:cell];
    int productRowIndex =[ indexPath row];
    int storeRowIndex = [[self.storesTableView indexPathForCell:cell] row];
    
    switch (selectedImageTag) {
        case PRODUCT_IMAGE_TAG:
        {
            
            NSLog(@"Selected Row:=%d",productRowIndex);
            if((allFeaturedProducts != nil) && ([allFeaturedProducts count] > 0))
            {
                [popUpViewPrdBtn setTitle:@"View Product" forState:UIControlStateNormal];
                [popUpView setHidden:NO];
                
                
                NSString *productImageUrl = [((NSDictionary *)[allFeaturedProducts objectAtIndex:productRowIndex]) valueForKey:PRODUCT_IMAGE_HVC];
                
                NSString *productDescription = [((NSDictionary *)[allFeaturedProducts objectAtIndex:productRowIndex]) valueForKey:PRODUCT_DESCRIPTION_HVC];
                
                NSString *productName= [((NSDictionary *)[allFeaturedProducts objectAtIndex:productRowIndex]) valueForKey:PRODUCT_NAME_HVC];
                
                selectedImageId = [((NSDictionary *)[allFeaturedProducts objectAtIndex:productRowIndex]) valueForKey:PRODUCT_ID_HVC];
                
                NSLog(@"ProductDescription=%@",productDescription);
                
                
                
                [popUpImage setImage:[[SDImageCache sharedImageCache] imageFromKey:productImageUrl]];
                [popUpName setText:productName];
                
               if (productDescription == (id)[NSNull null] )
               {
                   
               }
               else {
                   [popUpDescription setText:productDescription];

               }
                                }
            
            
        }
            break;
            
        case STORE_TOP_IMAGE:
        {
            
            
            if (toggleButton.tag == TOGGLE_STATE_ALL_STORES)
            {
                
                if((allStores != nil) && ([allStores count] > 0))
                {
                    
                    [popUpViewPrdBtn setTitle:@"View Products" forState:UIControlStateNormal];
                    
                    [popUpView setHidden:NO];
                    
                    NSString *topImageUrl = [((NSDictionary *)[allStores objectAtIndex:2*storeRowIndex]) valueForKey:STORE_IMAGE_HVC];
                    
                    NSString *storeDescription = [((NSDictionary *)[allStores objectAtIndex:2*storeRowIndex]) valueForKey:STORE_DESCRIPTION_HVC];
                    
                    NSString *storeName = [((NSDictionary *)[allStores objectAtIndex:2*storeRowIndex]) valueForKey:STORE_NAME_HVC];
                    
                    selectedImageName = [((NSDictionary *)[allStores objectAtIndex:2*storeRowIndex]) valueForKey:STORE_NAME_HVC];
                    
                    
                    selectedImageId = [((NSDictionary *)[allStores objectAtIndex:2*storeRowIndex]) valueForKey:STORE_ID_HVC];
                    
                    [popUpName setText:storeName];
                    [popUpImage setImage:[[SDImageCache sharedImageCache] imageFromKey:topImageUrl]];
                    if (storeDescription == (id)[NSNull null] )
                    {
                        
                    }
                    else {
                        [popUpDescription setText:storeDescription];
                    }
                        
                    NSLog(@"All stores");
                    
                    
                }
            }
            else {
                
                if((allFeaturedStores != nil) && ([allFeaturedStores count] > 0))
                {
                    
                    [popUpViewPrdBtn setTitle:@"View Products" forState:UIControlStateNormal];
                    
                    [popUpView setHidden:NO];
                    
                    NSIndexPath *indexPath = [self.productsTableView indexPathForCell:cell];
                    selectedRow = indexPath.row;
                    NSString *topImageUrl = [((NSDictionary *)[allFeaturedStores objectAtIndex:2*storeRowIndex]) valueForKey:STORE_IMAGE_HVC];
                    
                    NSString *storeDescription = [((NSDictionary *)[allFeaturedStores objectAtIndex:2*storeRowIndex]) valueForKey:STORE_DESCRIPTION_HVC];
                    
                    selectedImageName = [((NSDictionary *)[allFeaturedStores objectAtIndex:2*storeRowIndex]) valueForKey:STORE_NAME_HVC];
                    
                    
                    selectedImageId = [((NSDictionary *)[allFeaturedStores objectAtIndex:2*storeRowIndex]) valueForKey:STORE_ID_HVC];
                    
                    
                    
                    [popUpName setText:selectedImageName];
                    [popUpImage setImage:[[SDImageCache sharedImageCache] imageFromKey:topImageUrl]];
                    if (storeDescription == (id)[NSNull null] )
                    {
                        
                    }
                    else {
                        [popUpDescription setText:storeDescription];
                    }
                        
                }
                
                
            }
            
            
        }
            break;
            
        case STORE_BOTTOM_IMAGE:
        {
            
            
            
            if (toggleButton.tag == TOGGLE_STATE_ALL_STORES)
            {
                
                if((allStores != nil) && ([allStores count] > 0))
                {
                    [popUpViewPrdBtn setTitle:@"View Products" forState:UIControlStateNormal];
                    [popUpView setHidden:NO]; 
                    NSString *bottomStoreImageUrl = [((NSDictionary *)[allStores objectAtIndex:2*storeRowIndex+1]) valueForKey:STORE_IMAGE_HVC];
                    selectedImageId = [((NSDictionary *)[allStores objectAtIndex:2*storeRowIndex+1]) valueForKey:STORE_ID_HVC];
                    selectedImageName = [((NSDictionary *)[allStores objectAtIndex:2*storeRowIndex+1]) valueForKey:STORE_NAME_HVC];
                    NSString *storeDescription = [((NSDictionary *)[allStores objectAtIndex:2*storeRowIndex]) valueForKey:STORE_DESCRIPTION_HVC];
                    
                    
                    [popUpName setText:selectedImageName];
                    [popUpImage setImage:[[SDImageCache sharedImageCache] imageFromKey:bottomStoreImageUrl]]; 
                     if (storeDescription == (id)[NSNull null] )
                     {
                         
                     }
                     else {
                         [popUpDescription setText:storeDescription];
                     }
                        
                }
                
            }
            else {
                
                if((allFeaturedStores != nil) && ([allFeaturedStores count] > 0))
                {
                    [popUpViewPrdBtn setTitle:@"View Products" forState:UIControlStateNormal];
                    [popUpView setHidden:NO]; 
                    NSString *bottomStoreImageUrl = [((NSDictionary *)[allFeaturedStores objectAtIndex:2*storeRowIndex+1]) valueForKey:STORE_IMAGE_HVC];
                    selectedImageId = [((NSDictionary *)[allFeaturedStores objectAtIndex:2*storeRowIndex+1]) valueForKey:STORE_ID_HVC];
                    selectedImageName = [((NSDictionary *)[allFeaturedStores objectAtIndex:2*storeRowIndex+1]) valueForKey:STORE_NAME_HVC];
                    
                    NSString *storeDescription = [((NSDictionary *)[allFeaturedStores objectAtIndex:2*storeRowIndex]) valueForKey:STORE_DESCRIPTION_HVC];
                    
                    [popUpName setText:selectedImageName];
                    [popUpImage setImage:[[SDImageCache sharedImageCache] imageFromKey:bottomStoreImageUrl]]; 
                     if (storeDescription == (id)[NSNull null] )
                     {
                         
                     }
                    else
                        [popUpDescription setText:storeDescription];
                    
                }
                
            }
            
        }
            break;
            
        default:
            break;
    }
    
}


-(IBAction)returnKeyPressed:(id)sender
{
    [sender resignFirstResponder]; 
    UITextView *searchTextBox = (UITextView *)sender;
    NSString *searchText = searchTextBox.text;
    
    NSLog(@"Recent Searches in userDefaults=%@",recentSearchesArray);
    
    
    if (recentSearchesArray == nil)
    {
        recentSearchesArray = [[NSMutableArray alloc]init];
    }
    if(![recentSearchesArray containsObject:searchText])
    {
        if(searchText.length >0)
            [recentSearchesArray addObject:searchText];
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:recentSearchesArray];
        
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"recentSearchesKey"];
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
    
    
    ProductListViewController *cntrl = [[ProductListViewController alloc]initWithNibName:nil bundle:nil];
    if (searchTextBox.text.length > 0)
    {
        cntrl.productPrefix = searchTextBox.text;
        cntrl.navButtonTitle = @"Home";
        [self.navigationController pushViewController:cntrl animated:YES];
        [imageView removeFromSuperview];  // nav title image
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Enter some text for search to began" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

-(IBAction)scanButtonPressed:(id)sender
{
    
    //ScanViewController *scanViewCntrl = [[ScanViewController alloc]initWithNibName:nil bundle:nil];
    // [self presentModalViewController:scanViewCntrl animated:YES];
    
    
    ZBarReaderViewController *myReader = [ZBarReaderViewController new];
     myReader.readerDelegate = self;
    myReader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = myReader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    // present and release the controller
    [self presentModalViewController: myReader
                            animated: YES];
    
    //[self.navigationController pushViewController:myReader animated:YES];
    [self.navigationController.navigationBar setHidden:YES];
    [imageView removeFromSuperview];     // image in nav BAr
    
}


- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    //  myReader = reader;
    //myReader = reader;
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    
    
    
    scannedText = (NSMutableString *)symbol.data;
    
    ProductListViewController *cntrl = [[ProductListViewController alloc]initWithNibName:nil bundle:nil];
    cntrl.barcode = scannedText;
    cntrl.navButtonTitle = @"Home";
    [self.navigationController pushViewController:cntrl animated:YES];
    
    
    
    [self dismissModalViewControllerAnimated:YES];
    
    
    // UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Scanned Data" message:scannedText delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    //[alertView show];
}

-(IBAction)dismissPopUP:(id)sender
{
    [popUpView setHidden:YES];
    [backgroundView setHidden:YES];
    
}

-(IBAction)toggleButtonClick:(id)sender
{
    
    [toggleButton setEnabled:NO];
    if (toggleButton.tag == TOGGLE_STATE_FEATURED_STORES)   // currently showing only featured stores
    {
        // we wana show all stores
        [self getAllStores];
    }
    else {
        // all stores
        [allFeaturedStores removeAllObjects];
        
        [activityIndicator startAnimating];
        toggleButton.tag = TOGGLE_STATE_FEATURED_STORES;
        storeHeaderLabel.text = @"Featured Stores";
        [toggleButton setImage:[UIImage imageNamed:@"MoreStore_Btn"] forState:UIControlStateNormal];
        
        NSString *urlString = [NSString stringWithFormat: @"http://websupplements.net/projects/compare/rest/getFeaturedStores?withInDistance=%@&fromLatitude=%@&fromLongitude=%@",distance,latitude,longitude];
        
        NSLog(@"urlString=%@",urlString);
        dispatch_async(kBgQueue, ^{
            NSData* data = [NSData dataWithContentsOfURL: [NSURL URLWithString: 
                                                         urlString]];
            [self performSelectorOnMainThread:@selector(fetchedFeaturedStoresData:) 
                                   withObject:data waitUntilDone:YES];
        });    }
    
}

-(void)getAllStores
{
    toggleButton.tag = TOGGLE_STATE_ALL_STORES;        
    //[toggleButton setImage:@"Button_Search" forState:UIControlStateNormal];
    storeHeaderLabel.text = @"All Stores";
    [toggleButton setImage:[UIImage imageNamed:@"FeaturedStores_btn"] forState:UIControlStateNormal];
    
    [activityIndicator startAnimating];
    [allStores removeAllObjects];
    dispatch_async(kBgQueue, ^{
        
          NSString *urlString = [NSString stringWithFormat: @"http://websupplements.net/projects/compare/rest/getAllStores?withInDistance=%@&fromLatitude=%@&fromLongitude=%@",distance,latitude,longitude];
        NSLog(@"urlString=%@",urlString);
        NSDate *data =   [NSData dataWithContentsOfURL:[NSURL URLWithString: 
                                                        urlString]];
        [self performSelectorOnMainThread:@selector(fetchedAllStoresData:) 
                               withObject:data waitUntilDone:YES];
    });
    
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(IBAction)progressAlertView:(id)sender
{
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Select Range \n \n" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    [alertView setFrame:CGRectMake(20, 100, 200, 250)];
    
    progressLabel = [[UILabel alloc]initWithFrame:CGRectMake(215, 47, 70, 20)];
    [progressLabel setBackgroundColor:[UIColor clearColor]];
    
    mySlider = [[UISlider alloc]initWithFrame:CGRectMake(30, 47, 180, 20 )];
    [mySlider setMaximumValue:100];
    //[mySlider setm
    [mySlider setValue:sliderValue];
    
    [mySlider addTarget:self action:@selector(sliderChanged:) forControlEvents:UIControlEventValueChanged];
    [progressLabel setText:[NSString stringWithFormat:@"%d KM",sliderValue ]];
    progressLabel.textColor = [UIColor whiteColor];
    
    
    
    [alertView addSubview:progressLabel];
    [alertView addSubview:mySlider];
    [alertView show];
    
}
- (void)sliderChanged:(UISlider *)slider {
    sliderValue = (int)roundf(mySlider.value);
    progressLabel.text = [NSString stringWithFormat:@"%d KM", sliderValue];
}

-(IBAction)popUPAction:(id)sender
{
    [popUpView setHidden:YES];
    UIButton *popUpButton = (UIButton *)sender;
    //check if cancel or view product pressed
    switch (popUpButton.tag) 
    {
        case POP_PRODCT_STORE_TAG:
        {
            if(selectedImageTag == PRODUCT_IMAGE_TAG)
            {
                ProductDetailViewController *productDetailCntrl = [[ProductDetailViewController alloc]initWithNibName:nil bundle:nil];
                productDetailCntrl.productId = [selectedImageId intValue];
                productDetailCntrl.navButtonTitle = @"Home";
                
                [self.navigationController pushViewController:productDetailCntrl animated:YES];
                
            }
            else
            {
                StoreDetailViewController *storeDetailCntrl = [[StoreDetailViewController alloc]initWithNibName:nil bundle:nil];
                storeDetailCntrl.storeId = [selectedImageId intValue];
                storeDetailCntrl.storeName = selectedImageName;
                storeDetailCntrl.navButtonTitle = @"Home";
                [self.navigationController pushViewController:storeDetailCntrl animated:YES];
                
            }
            [imageView removeFromSuperview];
        }
            break;
            
        case POP_CANCEL_TAG:
        {
            
        }
            break;
            
            
        default:
            break;
    }
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [activityIndicator stopAnimating];
    NSLog(@"slider value=%f",[mySlider value]);
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%d",sliderValue] forKey:@"distance"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [rangeButton setTitle:[NSString stringWithFormat:@"Range - %d KM",sliderValue] forState:UIControlStateNormal];
    [rangeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [allFeaturedStores removeAllObjects];
    [allFeaturedProducts removeAllObjects];
    [allStores removeAllObjects];
    
    [productsTableView reloadData]; 
    [storesTableView reloadData];
    [activityIndicator startAnimating];
    [self callDefaultServicesAsynchronously];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)viewWillDisappear:(BOOL)animated
{
   // [recentSearchesArray removeAllObjects];
    /*[allFeaturedStores removeAllObjects];
    [allFeaturedProducts removeAllObjects];
    [allStores removeAllObjects];
    
    [productsTableView reloadData]; 
    [storesTableView reloadData];*/
}

@end
