//
//  RegisterViewController.m
//  IndianShopping
//
//  Created by Jagsonics on 21/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RegisterViewController.h"
#import "SBJson.h"

#define FIRST_NAME_FIELD_TAG    1
#define LAST_NAME_FIELD_TAG     2
#define EMAIL_FIELD_TAG         3
#define PASSWORD_FIELD_TAG      4
#define PHONE_FIELD_TAG         5
#define ADDRESS_FIELD_TAG       6

//-------------webservice parameters--------
#define MESSAGE_KEY_RGVC        @"message"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    isShiftedUp = NO;
    isShiftedDown = YES;
}


-(IBAction)registerButtonPressed:(id)sender
{
    
    [activityIndicator startAnimating];
    
    //blank field validation
    if ((emailTextField.text.length > 0) && (userPasswordTextField.text.length > 0) &&
        (firstNameTextField.text.length > 0) && (lastNameTextField.text.length > 0))
    {
        
        
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 
        if (![emailTest evaluateWithObject:emailTextField.text])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Not a valid email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [activityIndicator stopAnimating];
        }
        else 
        {
            //
            
            NSString * isReseller=@"0";
            if([resellerCheckBox isSelected])
            {
                isReseller=@"1";
            }
            dispatch_async(kBgQueue, ^{
                //first_name, last_name,userName, userPassword, phone_number, address
                NSString *urlStringWithParameters = [NSString stringWithFormat:@"%@?first_name=%@&last_name=%@&userName=%@&userPassword=%@&phone_number=%@&address=%@&is_reseller=%@",registerUserUrl,firstNameTextField.text,lastNameTextField.text,emailTextField.text,userPasswordTextField.text,phoneNoTextField.text,addressTextField.text,isReseller];        
                NSLog(@"url:%@",urlStringWithParameters);
                
                NSError *error;
                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString: urlStringWithParameters] options:0 error:&error];
                glbNetworkError = error;
                [self performSelectorOnMainThread:@selector(fetchedRegisterUserData:) 
                                       withObject:data waitUntilDone:YES];
            });
            
        }
        
        
    }
    else {
        UIAlertView *validationAlert = [[UIAlertView alloc]initWithTitle:nil message:@"All fields except phone number are mandatory" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [validationAlert show];
        [activityIndicator stopAnimating];
    }
    
    
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    return NO;
}

- (void)fetchedRegisterUserData:(NSData *)responseData {
    
    
    [activityIndicator stopAnimating];
    //parse out the json data
    
    if (glbNetworkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There is some problem with the network" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        
        
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSDictionary *dict = [parser objectWithData:responseData];
        NSLog(@"Dict=%@",dict);
        
        
        if (error == nil)
        {
            NSString *responeMessage = [dict valueForKey:MESSAGE_KEY_RGVC];           
            if([responeMessage isEqualToString:@"User added successfully!"])
            {
                UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"Registration Success" message:@"Move to Login Screen " delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
                [successAlert show];
            }
            else {
                UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"Registration Failed" message:@"Please try again later " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [successAlert show];
            }
            
        }
        else {
            
            NSLog(@"Parsing problem");
            
        }
        
        
        
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
            
        default:
            break;
    }
}
-(IBAction)returnKeyPressed:(id)sender
{
    UITextField *textField = (UITextField *)sender;
    [sender resignFirstResponder];
    UIResponder* nextResponder = [textField.superview viewWithTag:(textField.tag + 1)];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
        
        // Your code for checking it.
    }
}
-(void)shiftDown
{
    if (isShiftedUp)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:.5];
        shiftView.center = CGPointMake(shiftView.center.x, shiftView.center.y + 100);
        [UIView commitAnimations]; 
        isShiftedUp = NO;
    }  
}


-(IBAction)editingStarted:(id)sender
{
    UITextField* textField = (UITextField *)sender;
      [toolBar setHidden:YES];
    
    NSLog(@"text Field tag=%d",textField.tag);
    
    switch(textField.tag)
    {
        case FIRST_NAME_FIELD_TAG:
        {
            [self shiftDown];
        }
            break;
            
        case LAST_NAME_FIELD_TAG:
        {
           [self shiftDown]; 
        }
            break;
        case EMAIL_FIELD_TAG:
        {
          [self shiftDown];  
            //  shiftView.frame = CGRectMake(shiftView.center.x, shiftView.center. y - 50, shiftView.bounds.size.width, shiftView.bounds.size.height);
            
        }break;
        case PASSWORD_FIELD_TAG:
        {
            
            [self shiftDown];
            
            //shiftView.center = CGPointMake(shiftView.center.x, shiftView.center.y - 50);
            //shiftView.frame = CGRectMake(shiftView.center.x, shiftView.center. y - 50, shiftView.bounds.size.width, shiftView.bounds.size.height);
        }break;
        case PHONE_FIELD_TAG:
        {
              [toolBar setHidden:NO];
            if (!isShiftedUp)
            {
                
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:.5];
            shiftView.center = CGPointMake(shiftView.center.x, shiftView.center.y - 100);
            [UIView commitAnimations];
            isShiftedUp = YES;
            }
        }break;
        case ADDRESS_FIELD_TAG:
        {
        }break;
    }
    
}

-(IBAction)editingEnded:(id)sender
{
    UITextField* textField = (UITextField *)sender;
    
    NSLog(@"text Field tag=%d",textField.tag);
    
    switch(textField.tag)
    {
        case FIRST_NAME_FIELD_TAG:
        {
            
        }
            break;
            
        case LAST_NAME_FIELD_TAG:
        {
            
        }
            break;
        case EMAIL_FIELD_TAG:
        {
            
            //  shiftView.frame = CGRectMake(shiftView.center.x, shiftView.center. y - 50, shiftView.bounds.size.width, shiftView.bounds.size.height);
            
        }break;
        case PASSWORD_FIELD_TAG:
        {
        }break;
        case PHONE_FIELD_TAG:
        {
        }break;
        case ADDRESS_FIELD_TAG:
        {
            if (isShiftedUp)
            {
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDuration:.5];
                shiftView.center = CGPointMake(shiftView.center.x, shiftView.center.y + 100);
                [UIView commitAnimations]; 
                isShiftedUp = NO;
            }
           
        }break;
    }
    
}

-(IBAction)checkBoxClicked:(id)sender
{
    if([sender isSelected])
    {
        [sender setSelected:NO];
    }
    else {
        [sender setSelected:YES];
    }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(IBAction)doneButtonPressed:(id)sender
{
    
    [toolBar setHidden:YES];
    [[self.view viewWithTag:5] resignFirstResponder];
  
   /* if (isShiftedUp)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:.5];
        shiftView.center = CGPointMake(shiftView.center.x, shiftView.center.y + 100);
        [UIView commitAnimations]; 
        isShiftedUp = NO;
    }*/
    
     [[self.view viewWithTag:6]becomeFirstResponder];
}
@end
