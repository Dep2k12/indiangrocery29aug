//
//  AppDelegate.m
//  IndianShopping
//
//  Created by Jagsonics on 29/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"



#import "ScanViewController.h"
#import "LocationViewController.h"
#import "DisclaimerViewController.h"
#import "HomeViewController.h"
#import "SearchViewController.h"
#import "SplashScreenControllerViewController.h"
#import "ShoppingCartViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDImageCache.h>


@implementation AppDelegate

@synthesize window = _window;
@synthesize tabBarController = _tabBarController;
@synthesize isSaveListTapped;



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //What is wroing
    //update in delegate
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearDisk];
    [imageCache clearMemory];
    [imageCache cleanDisk];
    isSaveListTapped = NO;
    //7e9ed5a0   key
      self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    UIViewController  *homeViewController, *searchViewController, 
    *shoppingCartViewController,*locationViewController,* disclaimerViewController;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
    {
        
       
        homeViewController = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil]; 
       
        searchViewController = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];
        shoppingCartViewController = [[ShoppingCartViewController alloc]initWithNibName:@"ShoppingCartViewController" bundle:nil];
        locationViewController = [[LocationViewController alloc]initWithNibName:@"LocationViewController" bundle:nil];
        disclaimerViewController =[[DisclaimerViewController alloc]initWithNibName:@"DisclaimerViewController" bundle:nil];
        
        
        navigationControllerHome = [[UINavigationController alloc]initWithRootViewController:homeViewController];
        navigationControllerSearch = [[UINavigationController alloc]initWithRootViewController:searchViewController];
        navigationControllerShoppingCart = [[UINavigationController alloc]initWithRootViewController:shoppingCartViewController];
        navigationControllerLocation = [[UINavigationController alloc]initWithRootViewController:locationViewController];
        navigationControllerDisclaimer = [[UINavigationController alloc]initWithRootViewController:disclaimerViewController];
        
        
        
    } else {
       // viewController1 = [[FirstViewController alloc] initWithNibName:@"FirstViewController_iPad" bundle:nil];
       // viewController2 = [[SecondViewController alloc] initWithNibName:@"SecondViewController_iPad" bundle:nil];
    }
    
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:navigationControllerHome,navigationControllerSearch,navigationControllerShoppingCart,navigationControllerLocation,navigationControllerDisclaimer, nil];
    self.tabBarController.selectedIndex=0;
  
    if ([UITabBar instancesRespondToSelector:@selector(setSelectedImageTintColor:)])
    {
       self.tabBarController.tabBar.selectedImageTintColor = [UIColor redColor];
    }
   // self.tabBarController.tabBar.selectedImageTintColor = [UIColor redColor]; 
    
    
  //  SplashScreenControllerViewController *ssp = [[SplashScreenControllerViewController alloc]initWithNibName:nil bundle:nil];
    
    /*[self.tabBarController presentModalViewController:ssp animated:YES];
    [self.tabBarController performSelector:@selector(dismissModalViewControllerAnimated:) withObject:nil afterDelay:3.0];

    */
 
    self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];
       return YES;
}



- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isListFinalisedKey"];
}

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}
*/

- (UIColor *) colorWithHexString: (NSString *) hex  
{  
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];  
    
    // String should be 6 or 8 characters  
    if ([cString length] < 6) return [UIColor grayColor];  
    
    // strip 0X if it appears  
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];  
    
    if ([cString length] != 6) return  [UIColor grayColor];  
    
    // Separate into r, g, b substrings  
    NSRange range;  
    range.location = 0;  
    range.length = 2;  
    NSString *rString = [cString substringWithRange:range];  
    
    range.location = 2;  
    NSString *gString = [cString substringWithRange:range];  
    
    range.location = 4;  
    NSString *bString = [cString substringWithRange:range];  
    
    // Scan values  
    unsigned int r, g, b;  
    [[NSScanner scannerWithString:rString] scanHexInt:&r];  
    [[NSScanner scannerWithString:gString] scanHexInt:&g];  
    [[NSScanner scannerWithString:bString] scanHexInt:&b];  
    
    return [UIColor colorWithRed:((float) r / 255.0f)  
                           green:((float) g / 255.0f)  
                            blue:((float) b / 255.0f)  
                           alpha:1.0f];  
}




@end
