//
//  ProductListViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 18/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "CoreLocationController.h"


#define PRODUCTL_CATEGORY_NAME              @"category_name"
#define PRODUCTL_EAN_CODE                   @"ean_code"
#define PRODUCTL_IMAGE_URL                  @"main_image"
#define PRODUCTL_MANUFACTURER_NAME          @"manufacturer_name"
#define PRODUCTL_PRICE                      @"price"
#define PRODUCTL_PRODUCT_DESCRIPTION        @"product_description"
#define PRODUCTL_ID                         @"product_id"
#define PRODUCTL_PRODUCT_NAME               @"product_name"
#define PRODUCTL_SKU_CODE                   @"sku_code"
#define PRODUCTL_UPC_CODE                   @"upc_code"

@interface ProductListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate,CoreLocationControllerDelegate>

{
    NSString *productPrefix;
    NSString *navButtonTitle;
    NSString *barcode;
    id parentClass;
   
    NSArray *productsArray;
    IBOutlet UITableView *productsTableView;
    
    NSError *glbNetworkError;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    
    CLLocationDegrees latitude;   //double type
    CLLocationDegrees longitude;
    CoreLocationController* CLController;
    
    
    

    
}


@property(nonatomic,strong) NSString *productPrefix;
@property(nonatomic,strong) NSString *navButtonTitle;
@property(nonatomic,strong) NSString *barcode;
@property(nonatomic,strong) id parentClass;              // is this the  case of circular reference

@end
