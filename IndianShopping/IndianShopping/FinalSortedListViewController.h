//
//  FinalSortedListViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 25/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "NVMapViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "CoreLocationController.h"
#import <MQMapKit/MQMapKit.h>


@interface FinalSortedListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate,UITextFieldDelegate,CLLocationManagerDelegate,CoreLocationControllerDelegate,MQRouteDelegate>

{
    NSArray *sortedShoppingListArray;          // this array is not sorted, should be sorted in this class
    UIView *view;
    NSError *glbNetworkError;
    
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UITableView *myTable;
    
    CLLocationDegrees latitude;   //double type
    CLLocationDegrees longitude;
    CoreLocationController* CLController;
    IBOutlet UIButton *saveButton;
    IBOutlet UIButton *mapButton;
    
    NSMutableArray *points;
    MQMapView *map;
    NSMutableArray *routesArray;
    int routeLoadCount;
    NSMutableArray *unsortedArray;
   
    UITextField *alertTextField;
    NSString *listName;
    IBOutlet UILabel *listNameLabel;
    NSMutableData *responseData;

}

@property(nonatomic,retain) NSArray *unsortedArray;




-(IBAction)saveShoppingList:(id)sender;
@end
