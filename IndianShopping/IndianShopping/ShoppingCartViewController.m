//
//  ShoppingCartViewController.m
//  IndianShopping
//
//  Created by Jagsonics on 22/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ShoppingCartViewController.h"
#import "AppDelegate.h"
#import "Store.h"
#import "FinalSortedListViewController.h"
@interface ShoppingCartViewController ()

@end

@implementation ShoppingCartViewController

# pragma mark - life cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
                self.tabBarItem.image = [UIImage imageNamed:@"ShpoppingCart"];
        self.tabBarItem.title = @"Shopping";
        
                
        
  
    }
    
   
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController.navigationBar setTintColor:[((AppDelegate*)[[UIApplication sharedApplication]delegate ])colorWithHexString:@"C20907" ]];
    self.navigationItem.title = @"Shopping List";
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
   
        
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"shoppingListKey" ];    
    shoppingListArray = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (shoppingListArray == nil)
    {
        shoppingListArray = [[NSMutableArray alloc]initWithCapacity:1];
    }
    NSLog(@"Recent searcheArray from user defaults=%@",shoppingListArray);
     
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isListFinalisedKey"])
    {
        FinalSortedListViewController *cntrl = [[FinalSortedListViewController alloc]initWithNibName:nil bundle:nil];
        NSLog(@"shoppingListArray=%@",shoppingListArray);
        
        NSArray *sortedShoppingListArray = shoppingListArray;    
        cntrl.unsortedArray = sortedShoppingListArray;
        [self.navigationController pushViewController:cntrl animated:NO];
    }

    else {
        [self setTotalCost];
        [shoppingListTable reloadData];

    }
        

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)setTotalCost
{
    totalCost = 0;
    for(int i=0;i< [shoppingListArray count];i++)
    {
        int quatitiy = ((Store *)[shoppingListArray objectAtIndex:i]).storeProductQuantity ;
 
        int productPrice = [((Store *)[shoppingListArray objectAtIndex:i]).storeProductPrice intValue];
        
        totalCost = totalCost + (quatitiy * productPrice);
        
                
    }
    totalCostLabel.text = [NSString stringWithFormat:@"Total: $%d",totalCost];


}
# pragma mark - table View methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
       
    return [shoppingListArray count];      
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.textLabel.font = [UIFont systemFontOfSize:14.0];
            
    }
    
    NSInteger quantity = ((Store *)[shoppingListArray objectAtIndex:indexPath.row]).storeProductQuantity ;

    NSString *store = ((Store *)[shoppingListArray objectAtIndex:indexPath.row]).storeName;
    if(store.length > 15)
    {
          store = [store substringToIndex:14];
        store = [store stringByAppendingString:@"..."];
    }
      
    NSString *product =((Store *)[shoppingListArray objectAtIndex:indexPath.row]).storeProductName;
    
    if(product.length > 15)
    {
        product = [product substringToIndex:14];
        product = [product stringByAppendingString:@"..."];
    }
    NSInteger price = [((Store *)[shoppingListArray objectAtIndex:indexPath.row]).storeProductPrice intValue];
    
    
   
    cell.textLabel.text = [NSString stringWithFormat:@"%@      %@,    %d X $%d",store,product,quantity,price];
    
    
    // NSLog(@"recent search string=%@",[recentSearchArray objectAtIndex:0]);

    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - IBActions

-(IBAction)finalizeMyList:(id)sender
{
    if ([shoppingListArray count] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"No entry in the list" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
    else {
        FinalSortedListViewController *cntrl = [[FinalSortedListViewController alloc]initWithNibName:nil bundle:nil];
        
        NSArray *sortedShoppingListArray = shoppingListArray;     // perform sorting here
        cntrl.unsortedArray = sortedShoppingListArray;
        [self.navigationController pushViewController:cntrl animated:YES];
    }
    
    
    //call the service 
}

# pragma mark - rotation handler


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
