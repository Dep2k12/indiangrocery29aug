//
//  SearchViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 29/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductListViewController.h"
#import "ZBarSDK.h"

@interface SearchViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,ZBarReaderDelegate>
{
    IBOutlet UIView *searchBg;
    IBOutlet UITextView *searchBox;
    
    NSMutableArray *recentSearchArray;
    IBOutlet UITableView *recentSearchTable;
    
  
    
}

-(IBAction)returnKeyPressed:(id)sender;

@end
