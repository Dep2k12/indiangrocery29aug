//
//  ResellerViewController.m
//  IndianShopping
//
//  Created by Jagsonics on 09/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ResellerViewController.h"
#import "SBJson.h"
#import <QuartzCore/QuartzCore.h>


//-------------------------------setBuyerAccount Service-----------
#define SBA_IS_ACCOUNT_SET  @"isAccountSet"

@interface ResellerViewController ()

@end

@implementation ResellerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)fetchedAccountData:(NSData *)responseData {
    
    
    [activityIndicator stopAnimating];
    //parse out the json data
    
    if (glbNetworkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There is some problem with the network" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        
        //{"user_id":"23","email":"z@tmail.com","first_name":"zzzzzzz","last_name":"Zzzz"}
        //{"error":"Login Failed"}
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSDictionary *dict = [parser objectWithData:responseData];
        NSLog(@"Dict=%@",dict);
        
        
        if (error == nil)
        {
            NSString *isAcountSet = [dict valueForKey:SBA_IS_ACCOUNT_SET];
            if([isAcountSet isEqualToString:@"Yes"])
            {
                UIAlertView *alert = [ [UIAlertView alloc]initWithTitle:@"Success" message:@"User Account Updated" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
                
            }
            else {
                UIAlertView *alert = [ [UIAlertView alloc]initWithTitle:@"Error" message:@"Account not updated" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert show];
            }
                                    
                       
        }
        else {
            
            NSLog(@"Parsing problem");
            
        }
        
        
        
    }
    [saveButton setEnabled:YES];
}



-(IBAction)scanButtonPressed:(id)sender
{
    
    //ScanViewController *scanViewCntrl = [[ScanViewController alloc]initWithNibName:nil bundle:nil];
    // [self presentModalViewController:scanViewCntrl animated:YES];
    
    
    ZBarReaderViewController *myReader = [ZBarReaderViewController new];
    myReader.readerDelegate = self;
    myReader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = myReader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    // present and release the controller
    [self presentModalViewController: myReader
                            animated: YES];
    
    //[self.navigationController pushViewController:myReader animated:YES];
   // [self.navigationController.navigationBar setHidden:YES];
    
    
}


- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    //  myReader = reader;
    //myReader = reader;
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    
    NSString * scannedText = (NSMutableString *)symbol.data;
    
    [userIdfield setText:scannedText];
        
    
    
    [self dismissModalViewControllerAnimated:YES];
    
    
    // UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Scanned Data" message:scannedText delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    //[alertView show];
}

/*-(void)doneButtonPressed:(id)sender
{
    UITextField *textField = (UITextField *)sender;
    [sender resignFirstResponder];
    UIResponder* nextResponder = [textField.superview viewWithTag:(textField.tag + 1)];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
        
        // Your code for checking it.
    }
    
}*/

-(void)dismissKeyboard:(id)sender
{
    UIWindow* keyWindow = [[UIApplication sharedApplication] keyWindow];
     UIView* firstResponder = [keyWindow performSelector:@selector(firstResponder)];
     [firstResponder resignFirstResponder];
    
    
   UIResponder* nextResponder=[ keyWindow viewWithTag:firstResponder.tag + 1 ];
    
    //[sender resignFirstResponder];
    //UIResponder* nextResponder = [textField.superview viewWithTag:(textField.tag + 1)];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
        
        // Your code for checking it.
    }
    
    
}

-(IBAction)returnKeyPressed:(id)sender
{
    [sender resignFirstResponder];
}

-(IBAction)saveBtnPressed:(id)sender
{

    [activityIndicator startAnimating];
    [saveButton setEnabled:NO];
    NSString *reSellerId = [[NSUserDefaults standardUserDefaults] valueForKey:@"reSellerId"];
    NSString *userId = userIdfield.text;
    NSString *amount = amountField.text;
    
    if((userId == nil) || (amount == nil) || (userId.length ==0) || (amount.length == 0))
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Both fields are mandatory" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [activityIndicator stopAnimating];
        [saveButton setEnabled:YES];
    }
    else {
        
        dispatch_async(kBgQueue, ^{
        NSString *urlStringWithParameters = [NSString stringWithFormat:@"%@?user_id=%@&amount=%@&reseller_id=%@",setBuyerAccountDetailUrl,userId,amount,reSellerId];
        
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString: urlStringWithParameters] options:0 error:&error];
        glbNetworkError = error;
        [self performSelectorOnMainThread:@selector(fetchedAccountData:) 
                               withObject:data waitUntilDone:YES];
    });

    }
}
  
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    return YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    //[self.navigationItem setTitle:@"Account"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   }
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [view setHidden:YES];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
