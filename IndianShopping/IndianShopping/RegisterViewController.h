//
//  RegisterViewController.h
//  IndianShopping
//
//  Created by Jagsonics on 21/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController

{
    IBOutlet UITextField * firstNameTextField;
    IBOutlet UITextField * lastNameTextField;
    IBOutlet UITextField * emailTextField;
    IBOutlet UITextField * userPasswordTextField;
    IBOutlet UITextField * phoneNoTextField;
    IBOutlet UITextField * addressTextField;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UIView *shiftView;
    IBOutlet UIToolbar *toolBar;
    
    NSError *glbNetworkError;
    
    BOOL isShiftedUp;
    BOOL isShiftedDown;
    
    IBOutlet UIButton *resellerCheckBox;
    
}


-(IBAction)registerButtonPressed:(id)sender;
-(IBAction)returnKeyPressed:(id)sender;
-(IBAction)editingStarted:(id)sender;
-(IBAction)editingEnded:(id)sender;
-(IBAction)checkBoxClicked:(id)sender;

@end
