//
//  LocationViewController.m
//  IndianShopping
//
//  Created by Jagsonics on 29/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LocationViewController.h"
#import "AppDelegate.h"
#import "RegisterViewController.h"
#import "SBJson.h"
#import "SavedShoppingListsViewController.h"
#import "ResellerViewController.h"
#import "MyAccountViewController.h"


//LOGIN SUCCESS
#define LOGIN_USER_ID_KEY_LVC             @"user_id"
#define LOGIN_EMAIL_KEY_LVC               @"email"
#define LOGIN_FIRST_NAME_KEY_LVC          @"first_name"
#define LOGIN_LAST_NAME_KEY_LVC           @"last_name"

//LOGIC FAILED
#define LOGIN_ERROR_KEY_LVC               @"error"

//{"user_id":"23","email":"z@tmail.com","first_name":"zzzzzzz","last_name":"Zzzz"}
//{"error":"Login Failed"}
@interface LocationViewController ()

@end

@implementation LocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
   
        self.tabBarItem.image = [UIImage imageNamed:@"Myaccount_selected"];
        self.tabBarItem.title = @"My Account";
        
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSString *resellerId = [[NSUserDefaults standardUserDefaults]valueForKey:@"reSellerId"];
    NSString *userId = [[NSUserDefaults standardUserDefaults]valueForKey:@"userId"];
    
    if((resellerId == nil) && (userId == nil))
    {
        
        emailTextField.text=@"";
        userPasswordTextField.text=@"";
        [resellerCheckBox setSelected:NO];
        beforeLoginView.alpha = 1;    // show the login view
        resellerView.center = CGPointMake(-300, resellerView.center.y);  // move the reseller veiw

        [self.navigationController popToRootViewControllerAnimated:NO];
              // resellerView
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setTintColor:[((AppDelegate*)[[UIApplication sharedApplication]delegate ])colorWithHexString:@"C20907" ]];
    self.navigationItem.title = @"My Account";
    
    NSString *userId = [[NSUserDefaults standardUserDefaults]valueForKey:@"userId"];
    if(userId != nil)
    {
        //user is loggen in
        beforeLoginView.alpha = 0;
        [self animateViews];
    }
    NSString *resellerId = [[NSUserDefaults standardUserDefaults]valueForKey:@"reSellerId"];
    if(resellerId !=nil)
    {
        ResellerViewController *cntrl = [[ResellerViewController alloc]initWithNibName:nil bundle:nil];
        [self.navigationController pushViewController:cntrl animated:NO];
    }

    
}

-(void)animateViews
{
  //  NSLog(@"userID in default =%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userID"]);
    [UIView animateWithDuration:0.3 animations:^() {
        beforeLoginView.alpha = 0;
    }];
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1];
    afterLoginView.center = CGPointMake(self.view.center.x, afterLoginView.center.y);
    [UIView commitAnimations];
}

-(IBAction)loginButtonPressed:(id)sender
{
    
    [activityIndicator startAnimating];
    
    //blank field validation
    if ((emailTextField.text.length > 0) && (userPasswordTextField.text.length > 0))
    {
        
        
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 
        if (![emailTest evaluateWithObject:emailTextField.text])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Not a valid email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];  
            [activityIndicator stopAnimating];
        }
        else 
        {
            //
            NSString * isReseller=@"0";
            if([resellerCheckBox isSelected])
            {
                isReseller=@"1";
            }

            
            dispatch_async(kBgQueue, ^{
                //first_name, last_name,userName, userPassword, phone_number, address
                NSString *urlStringWithParameters = [NSString stringWithFormat:@"%@?userName=%@&userPassword=%@&is_reseller=%@",loginUserUrl,emailTextField.text,userPasswordTextField.text,isReseller];        
                NSLog(@"url:%@",urlStringWithParameters);
                
                NSError *error;
                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString: urlStringWithParameters] options:0 error:&error];
                glbNetworkError = error;
                [self performSelectorOnMainThread:@selector(fetchedLoginUserData:) 
                                       withObject:data waitUntilDone:YES];
            });
            
        }
        
        
    }
    else {
        UIAlertView *validationAlert = [[UIAlertView alloc]initWithTitle:nil message:@"Fields are mandatory" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [validationAlert show];
        [activityIndicator stopAnimating];

    }
    
    
}

/*- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    return NO;
}*/
-(void) animateToResellerOptions
{
    [UIView animateWithDuration:0.3 animations:^() {
        beforeLoginView.alpha = 0;
    }];
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1];
    resellerView.center = CGPointMake(self.view.center.x, resellerView.center.y);
    [UIView commitAnimations];

    
}
- (void)fetchedLoginUserData:(NSData *)responseData {
    
    
    [activityIndicator stopAnimating];
    //parse out the json data
    
    if (glbNetworkError != nil)
    {
        UIAlertView *networkAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There is some problem with the network" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [networkAlert show];
    }
    else {
        
        //{"user_id":"23","email":"z@tmail.com","first_name":"zzzzzzz","last_name":"Zzzz"}
        //{"error":"Login Failed"}
        NSError *error = nil;
        SBJsonParser *parser=[[SBJsonParser alloc] init];
        
        NSDictionary *dict = [parser objectWithData:responseData];
        NSLog(@"Dict=%@",dict);
        
        
        if (error == nil)
        {
            NSString *loginError = [dict valueForKey:LOGIN_ERROR_KEY_LVC];
            if ([loginError isEqualToString:@"Login Failed"])
            {
                UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"Login Failure" message:@"Data Invalid" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [successAlert show];
            }
            else {
                
                
                // Successful login 
                
                if([resellerCheckBox isSelected])
                {
                   /* ResellerViewController *cntrl = [[ResellerViewController alloc]initWithNibName:nil bundle:nil];
                    [self.navigationController pushViewController:cntrl animated:YES];*/
                    
                    [self animateToResellerOptions];
                    
                     NSString * userId  = [dict valueForKey:LOGIN_USER_ID_KEY_LVC];
                    [[NSUserDefaults standardUserDefaults] setValue:userId forKey:@"reSellerId"];
                    
                    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"userId"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                }
                else {
                    NSLog(@"userID in default =%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]);
                    NSString * userId  = [dict valueForKey:LOGIN_USER_ID_KEY_LVC];
                    
                    NSLog(@"userID from service=%@",userId);
                    
                    [[NSUserDefaults standardUserDefaults] setValue:userId forKey:@"userId"];
                    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"reSellerId"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    if(((AppDelegate *)[[UIApplication sharedApplication]delegate]).isSaveListTapped)
                    {
                        ((AppDelegate *)[[UIApplication sharedApplication]delegate]).isSaveListTapped = NO;
                        self.tabBarController.selectedIndex = 2;
                    }
                    [self animateViews];
                }
                
               
            }
            
        }
        else {
            
            NSLog(@"Parsing problem");
            
        }
        
        
        
    }
    
}

-(IBAction)myAccountPressed:(id)sender
{
    MyAccountViewController *cntrl = [[MyAccountViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:cntrl animated:YES];
}

-(IBAction)savedShoppingListPressed:(id)sender
{
    SavedShoppingListsViewController *cntrl = [[SavedShoppingListsViewController alloc]initWithNibName:nil bundle:nil];
    
    [self.navigationController pushViewController:cntrl animated:YES];
    
}

-(IBAction)goToreSellerController:(id)sender
{
     ResellerViewController *cntrl = [[ResellerViewController alloc]initWithNibName:nil bundle:nil];
     [self.navigationController pushViewController:cntrl animated:YES]; 
    
}


-(IBAction)logoutPressed:(id)sender
{
    emailTextField.text=@"";
    userPasswordTextField.text=@"";
    
   if([resellerCheckBox isSelected])
       
   {
       [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"reSellerId"];
       [[NSUserDefaults standardUserDefaults] synchronize]; 
       [UIView beginAnimations:nil context:NULL];
       [UIView setAnimationDuration:.6];
       resellerView.center = CGPointMake(-300, resellerView.center.y);
       [UIView commitAnimations];
       
       [UIView animateWithDuration:1.5 animations:^() {
           beforeLoginView.alpha = 1;
       }];
       
       [resellerCheckBox setSelected:NO];

   }
   else {
       [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"userId"];
       [[NSUserDefaults standardUserDefaults] synchronize];
       [UIView beginAnimations:nil context:NULL];
       [UIView setAnimationDuration:.6];
       afterLoginView.center = CGPointMake(-300, afterLoginView.center.y);
       [UIView commitAnimations];
       
       [UIView animateWithDuration:1.5 animations:^() {
           beforeLoginView.alpha = 1;
       }];

   }
   
    
    
    }
-(IBAction)registerButtonPressed:(id)sender
{
    RegisterViewController *cntrl = [[RegisterViewController alloc]initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:cntrl animated:YES]; 
}

-(IBAction)checkBoxClicked:(id)sender
{
    if([sender isSelected])
    {
        [sender setSelected:NO];
    }
    else {
        [sender setSelected:YES];
    }
}

-(IBAction)returnKeyPressed:(id)sender
{
    
    UITextField *textField = (UITextField *)sender;
    [sender resignFirstResponder];
    UIResponder* nextResponder = [textField.superview viewWithTag:(textField.tag + 1)];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
        
        // Your code for checking it.
    }

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
