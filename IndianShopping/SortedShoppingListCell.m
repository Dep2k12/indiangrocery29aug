//
//  SortedShoppingListCell.m
//  IndianShopping
//
//  Created by Jagsonics on 25/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SortedShoppingListCell.h"

@implementation SortedShoppingListCell
@synthesize store;
@synthesize product;
@synthesize quantity;
@synthesize price;
@synthesize saving;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
